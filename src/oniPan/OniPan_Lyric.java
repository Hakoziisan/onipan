package oniPan;

public class OniPan_Lyric {

	/**[connecter]
	 * （入力文 or[parts],
	 *   入力文 or[parts],）
	 *
	 * @param msg
	 * @param msg2
	 * @return
	 */
	public static String repeatMessage(String msg,String msg2) {
		String repeatMsg="";
			repeatMsg+=(msg+msg2);
			return repeatMsg;
	}

	/**[connecter]
	 * （入力文 or[parts],リピート数）
	 *
	 * @param msg
	 * @param count
	 * @return
	 */
	public static String repeatMessage(String msg,int count) {
		String repeatMsg="";
		for(int i=0;i<count;i++){
			repeatMsg+=msg;
		}
			return repeatMsg;
	}

	/**[parts]
	 * 改行用
	 *
	 * @return
	 */
	public static String newLine (){
		return "\n";
	}
	/**[parts]
	 * 歌詞「鬼のパンツはいいパンツ」
	 *
	 * @return
	 */
	public static String goodPants (){
		return "鬼のパンツはいいパンツ";
	}

	/**[parts]
	 * 歌詞「強いぞ強いぞ」
	 *
	 * @return
	 */
	public static String strongs (){
		return "強いぞ強いぞ";
	}

	/**[parts]
	 * 歌詞「鬼の毛皮でできている」
	 *
	 * @return
	 */
	public static String pantsMaterial (){
		return "鬼の毛皮でできている";
	}

	/**[parts]
	 * 歌詞「○年はいてもやぶれない」
	 * （年数）
	 *
	 * @param years
	 * @return
	 */
	public static String pantsLifeTime (int years){
		return years+"年はいてもやぶれない";
	}

	/**[parts]
	 * 歌詞「はこうはこう鬼のパンツ」
	 *
	 * @return
	 */
	public static String fittingPants (){
		return "はこうはこう鬼のパンツ";
	}

	/**[parts]
	 * 歌詞「あなたも」
	 *
	 * @return
	 */
	public static String guys (){
		return "あなたも";
	}

	/**[parts]
	 * 歌詞「みんなではこう鬼のパンツ」
	 * @return
	 */
	public static String everyone(){
		return "みんなではこう鬼のパンツ";
	}

	public static void execute(){
		String lyric="";
		//一行目
		lyric+=repeatMessage(goodPants(),newLine());
		lyric+=repeatMessage(strongs(), 2);
		lyric+=newLine();
		//二行目
		lyric+=repeatMessage(pantsMaterial(),newLine());
		lyric+=repeatMessage(strongs(), 2);
		lyric+=newLine();
		//三行目
		lyric+=repeatMessage(pantsLifeTime(5), newLine());
		lyric+=repeatMessage(strongs(), 2);
		lyric+=newLine();
		//四行目
		lyric+=repeatMessage(pantsLifeTime(10), newLine());
		lyric+=repeatMessage(strongs(), 2);
		lyric+=newLine();
		//五行目
		lyric+=repeatMessage(fittingPants(), newLine());
		lyric+=repeatMessage(fittingPants(), newLine());
		//六行目
		lyric+=repeatMessage(guys(), 4);
		lyric+=newLine();
		lyric+=everyone();
		System.out.println(lyric);

	}
}
